import unittest

from DateYearConverter import DateYearConverter

NUMBER_OF_DAYS_IN_LEAP_YEAR = 366

NUMBER_OF_DAYS_IN_YEAR = 365


class DateYearConverterTest(unittest.TestCase):

    def test_first_day_of_2006(self):
        self.assert_correct_year(2006, 1)

    def test_last_day_of_2006(self):
        self.assert_correct_year(2006, NUMBER_OF_DAYS_IN_YEAR)

    def test_first_day_of_2007(self):
        self.assert_correct_year(2007, NUMBER_OF_DAYS_IN_YEAR + 1)

    def test_last_day_of_2007(self):
        self.assert_correct_year(2007, NUMBER_OF_DAYS_IN_YEAR * 2)

    def test_first_day_of_2008(self):
        self.assert_correct_year(2008, NUMBER_OF_DAYS_IN_YEAR * 2 + 1)

    def test_last_day_of_2008_leap_year(self):
        self.assert_correct_year(2008, NUMBER_OF_DAYS_IN_YEAR * 2 + NUMBER_OF_DAYS_IN_LEAP_YEAR)

    def test_first_day_of_2009(self):
        self.assert_correct_year(2009, NUMBER_OF_DAYS_IN_YEAR * 2 + NUMBER_OF_DAYS_IN_LEAP_YEAR + 1)

    def test_last_day_of_2009(self):
        self.assert_correct_year(2009, NUMBER_OF_DAYS_IN_YEAR * 3 + NUMBER_OF_DAYS_IN_LEAP_YEAR)

    def test_first_day_of_2010(self):
        self.assert_correct_year(2010, NUMBER_OF_DAYS_IN_YEAR * 3 + NUMBER_OF_DAYS_IN_LEAP_YEAR + 1)

    def assert_correct_year(self, expected_year, given_days):
        converter = DateYearConverter()
        year = converter.get_years_from_month(given_days)
        self.assertEqual(year, expected_year)


if __name__ == '__main__':
    unittest.main()
