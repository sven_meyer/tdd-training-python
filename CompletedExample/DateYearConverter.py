NUMBER_OF_DAYS_IN_YEAR = 365
NUMBER_OF_DAYS_IN_LEAP_YEAR = 366


class DateYearConverter:

    def __init__(self):
        pass

    def get_years_from_month(self, given_days):
        year = 2006
        number_of_days_this_year = NUMBER_OF_DAYS_IN_YEAR

        while given_days > number_of_days_this_year:
            year += 1
            given_days -= number_of_days_this_year
            number_of_days_this_year = self.number_of_days_next_year(year)

        return year

    def number_of_days_next_year(self, year):
        if self.is_leap_year(year):
            return NUMBER_OF_DAYS_IN_LEAP_YEAR
        else:
            return NUMBER_OF_DAYS_IN_YEAR

    @staticmethod
    def is_leap_year(year):
        return 2008 == year
